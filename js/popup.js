$( document ).ready(function() {
    $(' .modal-close, .shadow, .shadow_6, .butn-white').on('click', function(e) {
        if(e.target.className !== "modalMine"){
            $('.modalBlock').removeClass('is-visible');
            $('.modalMine').removeClass('is-visible');
            $('body').removeClass('modal-open');
        }
    });
    
    function showModal_1(){
        $('.modalMine').removeClass('is-visible');
        $('body').addClass('modal-open');
        $('.modalBlock, .modal_1').addClass('is-visible');
    }
    function showModal_2(){
        $('.modalMine').removeClass('is-visible');
        $('body').addClass('modal-open');
        $('.modalBlock, .modal_2').addClass('is-visible');
    }
    function showModal_3(){
        $('.modalMine').removeClass('is-visible');
        $('body').addClass('modal-open');
        $('.modalBlock, .modal_3').addClass('is-visible');
    }
    function showModal_4(){
        $('.modalMine').removeClass('is-visible');
        $('body').addClass('modal-open');
        $('.modalBlock, .modal_4').addClass('is-visible');
    }
    function showModal_5(){
        $('.modalMine').removeClass('is-visible');
        $('body').addClass('modal-open');
        $('.modalBlock, .modal_5').addClass('is-visible');
    }
    function showModal_6(){
        $('.modalMine').removeClass('is-visible');
        $('body').addClass('modal-open');
        $('.modalBlock, .modal_6').addClass('is-visible');
    }
    
    $('#btnPop_1').click(function (){showModal_1()});
    $('#btnPop_2').click(function (){showModal_2()});
    $('#btnPop_3').click(function (){showModal_3()});
    $('#btnPop_4').click(function (){showModal_4()});
    $('#btnPop_5').click(function (){showModal_5()});
    $('#btnPop_6').click(function (){showModal_6()});
});